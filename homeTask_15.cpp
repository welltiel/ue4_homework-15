﻿#include <iostream>

void FindOddOrEvenNumbers(int Limit, bool IsOdd)
{
    for (int i = 0 + IsOdd; i <= Limit; i += 2)
    {
        std::cout << i << "\n";
    }
}

int main()
{
    int Limit = 100;
    FindOddOrEvenNumbers(Limit, true);
}
